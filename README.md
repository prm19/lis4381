>
>



# LIS4381

## Payne Moore

###  Requirements:
*Course works links:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
	- Install AMPPS, JDK, Android Studio
	- GIT Commands
	- Tutorial Link

2. [A2 README.md](a2/README.md "My A2 README.md file")
	- Create Recipe app on Android Studio
	- Sreenshots of App
	- Skill Sets 1-3 

3. [A3 README.md](a3/README.md "My A3 README.md file")
	- Screenshots of running Concert Tickets app on Android Studio
	- Screenshots of ERD with data entries
	- Skill Sets 4-6

4. [P1 README.md](p1/README.md "My P1 README.md file")
	- Create business card on Android Studio
	- Implement two interfaces with design components
	- Skill Sets 7-9

5. [A4 README.md](a4/README.md "My A4 README.md file")
	- Create Main page for LIS4381 Portal using bootstrap
	- Create A1-A5 for bootstrap
	- Screenshot of working site
	- Failed and successful data validation
	- Link to Local Host
	- Skill Sets 10-12

6. [A5 README.md](a5/README.md "My A5 README.md file")
	- Use server-side data validation for data entries
	- Screenshot of error message
	- Screenshots of successful data validation and entry
	- Skill Sets 13-15

7. [P2 README.md](p2/README.md "My P2 README.md file")
	- Create edit function with server side validation
	- Create delete function with server side validation
	- Create RSS feed





