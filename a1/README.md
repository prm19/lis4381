> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# lis4381

## Payne Moore

### Assignment #1 Requirements:

*Sub-Heading:*

1. Ordered-list items of Git
2. Screenshot of running AMPPS, Java Hello, Android Studio Code
3. Tutorial Links

#### README.md file should include the following items:

* Bullet-list items
* 
* 
* 

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init :  Create an empty Git repository or reinitialize an existing one
2. git status : Show the working tree status
3. git add : Add file contents to the index
4. git commit : Record changes to the repository
5. git push : Update remote refs along with associated objects
6. git pull : Fetch from and integrate with another repository or a local branch
7. git switch : Switch branches

#### Assignment Screenshots:

*Screenshot of AMPPS running http://localhost*:

![AMPPS Installation Screenshot](AMPPS.png)

*Screenshot of running java Hello*:

![JDK Installation Screenshot](javaSS.png)

*Screenshot of Android Studio - My First App*:

![Android Studio Installation Screenshot](AndroidStudioSS.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

