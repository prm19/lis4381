
# lis4381

## Payne Moore

### Assignment #2 Requirements:

*Sub-Heading:*

1. Screenshot of running user interface 1
2. Screenshot of running user interface 2
3. Skill Sets 1-3
 


#### Assignment Screenshots:

*Screenshot of running app*:

![Interface 1 Screenshot](img2/RecipePage1.png)

*Screenshot of running app*:

![Interface 2 Screenshot](img2/RecipePage2.png)




#### Skill Sets:

*Even or odd - Skill Set 1*

![Running Even or Odd program Screenshot](img2/Even_or_Odd.png)

*Largest Number - Skill Set 2*

![Running Largest Number Program](img2/LargestNumber1.png)
![Running Largest Number Program 2](img2/LargestNumber2.png)

*Arrays and Loops - Skill Set 3*
![Running Arrays and Loops Program](img2/Arrays_And_Loops.png)




