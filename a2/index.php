<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
		<meta name="author" content="Payne Moore">
    <link rel="icon" href="favicon.ico">

		<title>LIS4381 - Assignment2</title>		
		<?php include_once("../css/include_css.php"); ?>			
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<p><strong>Description:</strong></p>
					<p>1. Screenshot of running user interface 1</p>
					<p>2. Screenshot of running user interface 2 </p>
					<p>3. Skill Sets 1-3</p>
				</p>

				<h4>Interface 1</h4>
				<img src="img2/RecipePage1.png" class="img-responsive center-block" alt="Interface 1">

				<h4>Interface 2</h4>
				<img src="img2/RecipePage2.png" class="img-responsive center-block" alt="Interface 2">

				<h4>Skill Set 1 - Even or Odd</h4>
				<img src="img2/Even_or_Odd.png" class="img-responsive center-block" alt="SS1">
				
				<h4>Skill Set 2 - Largest Number</h4>
				<img src="img2/LargestNumber1.png" class="img-responsive center-block" alt="SS2">
				<img src="img2/LargestNumber2.png" class="img-responsive center-block" alt="SS2">

				<h4>Skill Set 3 - Arrays and Loops</h4>
				<img src="img2/Arrays_And_Loops.png" class="img-responsive center-block" alt="SS3">



				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
