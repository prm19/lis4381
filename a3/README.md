# lis4381

## Payne Moore

### Assignment #3 Requirements:

*Sub-Heading:*

1. Screenshot of running user interface 2 (Concert Ticket App)
2. Screenshot of running user interface 2 (Concert Ticket App)
3. Screenshot of ERD
4. Screenshots of Data entry
5. Links to MWB and SQL files
6. Skill Sets 4-6
 


#### Assignment Screenshots:

| Payne Moore   | Android Studio | 
| :---          |         ---:   |
| *Screenshot of Interface 1*:        | *Screenshot of Interface 2*:      | 
| ![Interface 1 Screenshot](img3/Interface1.png)     | ![Interface 2 Screenshot](img3/Interface2.png)     |



#### ERD Screenshots:

*Screenshot of ERD*:

![ERD Screenshot](img3/erd.png)

*Screen shot of data entries using queries*:

*- Screenshot of Pet data entries*
![Screenshot dataPet](img3/pet.png) 
*- Screenshot of Customer data entries* 
![Screenshot dataCustomer](img3/customer.png)
*- Screenshot of Petstore data entries*
![dataPetstore](img3/petstore.png) 

*Links to mwb files*
[a3.mwb](https://bitbucket.org/prm19/lis4381/src/master/a3/docs/a3.mwb)
[a3.sql](https://bitbucket.org/prm19/lis4381/src/master/a3/docs/a3.sql)


#### Skill Sets:

|*Skill Sets 4-6*| 
|:---:           | 
|*Decision Structures - Skill Set 4*|                
|![Running Decision Structures Screenshot](img3/Decision_Structure.png)|
|*Nested Structures - Skill Set 5* |
|![Running Nested Structures Program](img3/Nested_Structures.png) |
|*Methods  - Skill Set 6* |
|![Running Methods Program](img3/Methods.png)
