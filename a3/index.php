<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
		<meta name="author" content="Payne Moore">
    <link rel="icon" href="favicon.ico">

		<title>LIS4381 - Assignment2</title>		
		<?php include_once("../css/include_css.php"); ?>			
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<p><strong>Description:</strong></p>
					<p>1. Screenshot of running user interface 2 (Concert Ticket App)</p>
					<p>2. Screenshot of running user interface 2 (Concert Ticket App) </p>
					<p>3. Screenshot of ERD</p>
					<p>4. Screenshots of Data entry</p>
					<p>5. Links to MWB and SQL files</p>
					<p>6. Skill Sets 4-6</p>
				</p>

				<h4>Interface 1</h4>
				<img src="img3/Interface1.png" class="img-responsive center-block" alt="Interface 1">

				<h4>Interface 2</h4>
				<img src="img3/Interface2.png" class="img-responsive center-block" alt="Interface 2">

				<h4>Screenshot of ERD</h4>
				<img src="img3/erd.png" class="img-responsive center-block" alt="SS1">
				
				<h4>Screenshot of data entries</h4>
				<img src="img3/pet.png" class="img-responsive center-block" alt="pet data">
				<img src="img3/customer.png" class="img-responsive center-block" alt="customer data">
				<img src="img3/petstore.png" class="img-responsive center-block" alt="petstore data">

				<h4>Links to DATA files</h4>
				<h3><a href="https://bitbucket.org/prm19/lis4381/src/master/a3/docs/a3.mwb"> MWB FILE</a></h3>
				<h3><a href="https://bitbucket.org/prm19/lis4381/src/master/a3/docs/a3.sql"> SQL FILE</a></h3>

				<h4>Skill Sets 4-6</h4>
				<h3>Skill Set 4 - Decision Structures</h3>
				<img src="img3/Decision_Structure.png" class="img-responsive center-block" alt="SS4">
				<h3>Skill Set 5 - Nested Structures</h3>
				<img src="img3/Nested_Structures.png" class="img-responsive center-block" alt="SS5">
				<h3>Skill Set 6 - Methods</h3>
				<img src="img3/Methods.png" class="img-responsive center-block" alt="SS6">



				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
