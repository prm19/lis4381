# lis4381

## Payne Moore

### Assignment #4 Requirements:

*Sub-Heading:*

1. Screenshot of My Main page
2. Screenshot of failed data Validation
3. Screenshot of successful data Validation
3. Skill Sets 10-12
 


#### Assignment Screenshots:
*Main Page*
![main page](img/mainpage.png)

*Failed Data Validation*
![failed](img/failedvalid.png)

*Successful Data Validation*
![sucess](img/sucessfulvalid.png)


[WebApp](https://bitbucket.org/prm19/lis4381/src/master/a4/docs/local.webarchive)




#### Skill Sets:

|Skill Sets 10-12| 
|:---:           | 
|- Skill Set 10: Array Lists|                
|![ss10](img/Q10.png)|
| - Skill Set 11: Alpha, Numeric, and Special Characters||
|![ss11](img/Q11.png) |
|  - Skill Set 12: Temperature Conversion|
|![ss12](img/Q12.png)
