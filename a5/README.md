# lis4381

## Payne Moore

### Assignment #5 Requirements:

*Sub-Heading:*

1. Screenshot of running Data Entries
2. Screenshot of adding a new petstore with invalid phone number
3. Screenshot of error message
4. Screenshots of adding a new petstore with correct information
5. Screenshot of updated petstore data entries
6. Skill Sets 13-15
 


#### Assignment Screenshots:

| Payne Moore   | Bootstrap Site | 
| :---          |         ---:   |
| *Screenshot Data*:        | *Screenshot of new entry with invalid phone number*:      | 
| ![Interface 1 Screenshot](img/data_entries.png)     | ![Interface 2 Screenshot](img/unsuccess_validation1.png)     |



#### Error Message



![Error](img/unsuccess_validation2.png)

#### Screenshots of successful data entries:

*Screenshot of Petstore data entry with valid information*
![Screenshot 1](img/Success_Validation1.png) 
*Screenshot of Customer updated petstore table* 
![Screenshot dataCustomer](img/new_Data_Entry.png)




#### Skill Sets:

|*Skill Sets 13-15*| 
|:---:           | 
|*Sphere Volume - Skill Set 13*|                
|![Running Sphere Volume Screenshot](img/spherevolume.png)|
|*Simple Calculator (PHP) - Skill Set 14* |
|![Running calculator Program](img/add1.png) |
|![Running calculator Program](img/add2.png) |
|![Running calculator Program](img/divide1.png) |
|![Running calculator Program](img/divide2.png) |
|*Read Write File  - Skill Set 15* |
|![Running read write Program](img/readwrite1.png)
|![Running calculator Program](img/readwrite2.png) |
