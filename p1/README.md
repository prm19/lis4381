# lis4381

## Payne Moore

### Project 1 Requirements:

1. Screenshot of running user interface 1 (Business Card App)
2. Screenshot of running user interface 2 (Business Card App)
3. Skill Sets 7-9
 


#### Assignment Screenshots:
|*Android Studio*|
|:---:|
|*1. Screenshot of Interface 1 & 2:*|
|![Interface 1 & 2 Screenshot](img/Interfaces.png)|





#### Skill Sets:

|*Skill Sets 7-9*| 
|:---:           | 
|*Random Arrays - Skill Set 7*|                
|![Running RANDOM ARRAY](img/Q7.png)|
|*Largest of Three Numbers - Skill Set 8* |
|![Running Largest of three numbers Program](img/Q8.png) |
|*Run-Time Arrays  - Skill Set 9*|
|![Running RunTime Program](img/Q9.png)
