<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="description" content="My online portfolio that illustrates skills acquired while working through various project requirements.">
		<meta name="author" content="Payne Moore">
    <link rel="icon" href="favicon.ico">

		<title>LIS4381 - Assignment2</title>		
		<?php include_once("../css/include_css.php"); ?>			
  </head>

  <body>

		<?php include_once("../global/nav.php"); ?>
		
		<div class="container">
			<div class="starter-template">
				<div class="page-header">
					<?php include_once("global/header.php"); ?>	
				</div>
				<p class="text-justify">
					<p><strong>Description:</strong></p>
					<p>1. Screenshot of running user interface 1 (Business Card App)</p>
					<p>2. Screenshot of running user interface 2 (Business Card App)</p>
					<p>3. Skill Sets 7-9</p>
				</p>

				<h4>Interfaces</h4>
				<img src="img/Interfaces.png" class="img-responsive center-block" alt="Interfaces">


				<h4>Skill Set 7 - Random Array</h4>
				<img src="img/Q7.png" class="img-responsive center-block" alt="SS7">
				
				<h4>Skill Set 8 - Largest of Three Numbers</h4>
				<img src="img/Q8.png" class="img-responsive center-block" alt="SS8">


				<h4>Skill Set 9 - Run-Time Arrays</h4>
				<img src="img/Q9.png" class="img-responsive center-block" alt="SS9">



				<?php include_once "global/footer.php"; ?>

			</div> <!-- starter-template -->
    </div> <!-- end container -->

		<!-- Bootstrap JavaScript
				 ================================================== -->
		<!-- Placed at end of document so pages load faster -->		
		<?php include_once("../js/include_js.php"); ?>			
  </body>
</html>
