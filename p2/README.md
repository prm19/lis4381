# lis4381

## Payne Moore

### Project #2 Requirements:


1. Screenshot of main page
2. P2 Main page
3. Screenshot of editing petstore with wrong city input
4. Screenshot of succesful validation after edit
5. Screenshots of deleting a petstore
6. Screenshot RSS feed

 


#### Assignment Screenshots:


| Payne Moore   |  
| :---          | 
| *Main Page*:       | 
| ![Interface 1 Screenshot](img/OnlinePortfolio.png)     |
| *Data entries*|
| ![Interface 2 Screenshot](img/indexphp.png)     |



#### Error Message


![Error](img/edit_failed_1.png)
![Error](img/Edit_failed_2.png)

#### Screenshots of successful data edits:

*Screenshot of Petstore data entry with valid information*
![Screenshot 1](img/Edit_success.png) 


#### Screenshots of deleting a record
![Screenshot delete1](img/Delete1.png)
![Screenshot delete2](img/Delete2.png)




#### RSS Feed:

|*Skill Sets 13-15*| 
|:---:           | 
|*USAToday RSS Feed*|                
|![Running Sphere Volume Screenshot](img/rss.png)|