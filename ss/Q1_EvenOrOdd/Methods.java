import java.util.Scanner;

public class Methods 
{
    public static void getRequirements()
    {
        System.out.println("Developer: Payne Moore");
        System.out.println("Program evaluates integers as even or odd.");
        System.out.print("Note: Program does not check for non-numeric characters");

        System.out.println();

    }
    //Create evaluateNumber method
    public static void evaluateNumber()
    {
        //Declare variable y 
        //Create prompt and scanner object for input
        int y=0;
        System.out.print("Enter integer: ");
        Scanner sc = new Scanner(System.in);
        y = sc.nextInt();

        //Create if else statement
        //Evaluate even or odd with Mod
        if (y % 2 == 0)
        {
            System.out.println(y + " is an even integer.");
        }
        else{
            System.out.println(y + " is an odd integer.");
        }
        
    }
}
