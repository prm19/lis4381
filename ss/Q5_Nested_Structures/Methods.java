import java.util.Scanner;

public class Methods 
{
    private static int i;
    public static void getRequirements()
    {
        System.out.println("\nDeveloper: Payne Moore");
        System.out.println("Program searches user-entered integer w/in array of integers.");
        System.out.print("Create an array with the following values: 3, 2, 4, 99, -1, -5, 3, 7");

        System.out.println();

    }
    //Create evaluateNumber method
    public static void evaluateNumber(){

        Scanner sc = new Scanner(System.in);

        int[] test = { 3, 2, 4, 99, -1, -5, 3, 7};
        System.out.println("\nArray Length: " + test.length);

        System.out.println("Enter search value: ");
        int val = sc.nextInt();

        for (int i = 0; i < test.length; i++)
        {
            if (test[i] == val)
            {
                System.out.println(val + " is found at index " + i);
            }
            else
            {
                System.out.println(val + " is *not* found at index " + i);
            }
        }
    }
    
}
